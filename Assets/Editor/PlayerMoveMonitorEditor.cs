﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(PlayerMoveMonitor))]
public class PlayerMoveMonitorEditor : Editor {
	public override void OnInspectorGUI(){
		DrawDefaultInspector ();
		PlayerMoveMonitor myScript = (PlayerMoveMonitor)target;

		if(GUILayout.Button("Create New Replay")){
			myScript.CreateNewReplay ();
		}

		if(GUILayout.Button("Print Replay's moves")){
			myScript.PlayMoves ();
		}
	}


}
