﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridScript : MonoBehaviour {

	public Toggle gridToggle;
	public GameObject _grid;
	public Material gridON;
	public Material gridOFF;
	public Material gridReplay;

	public void Init(){
		if(PlayerPrefs.GetInt("ShowGrid") == 1){
			showGrid (true);
			gridToggle.isOn = true;
		} else {
			showGrid (false);
			gridToggle.isOn = false;
		}
	}

	public void showGrid(bool val){
		SetGridStatus (val);
		if(_grid != null){
			if(val){
				_grid.GetComponent<MeshRenderer> ().material = gridON;

			} else {
				_grid.GetComponent<MeshRenderer> ().material = gridOFF;
			}
		}
	}

	public void SetGridStatus(bool ON){
		if(ON){
			PlayerPrefs.SetInt ("ShowGrid", 1);
		} else {
			PlayerPrefs.SetInt ("ShowGrid", 0);
		}
	}

	public void SetGridReplay(bool ON){
		if(ON){
			_grid.GetComponent<MeshRenderer> ().material = gridReplay;
		} else {
			if(_grid != null){
				if(PlayerPrefs.GetInt("ShowGrid") == 1){
					_grid.GetComponent<MeshRenderer> ().material = gridON;

				} else {
					_grid.GetComponent<MeshRenderer> ().material = gridOFF;
				}
			}
		}

	}


}
