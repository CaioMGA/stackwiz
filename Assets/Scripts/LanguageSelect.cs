﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelect : MonoBehaviour {
	public TextAsset pt_br;
	public TextAsset en;
	public TextAsset jp;
	public TextAsset it;

	string lang;

	void Start(){
		DontDestroyOnLoad (this.gameObject);
		GetCurLang ();
	}

	public TextAsset GetTextAsset(){
		switch(lang){
		case "pt-br":
			return pt_br;
		case "jp":
			return jp;
		case "it":
			return it;
		default:
			return en;
		}
	}

	public void GetCurLang(){
		string l = PlayerPrefs.GetString ("curLang");
		if(l.Length < 2){
			l = "en";
		}
		lang = l;
	}

	public void SetCurLang(string l){
		PlayerPrefs.SetString ("curLang", l);
		lang = l;
	}
}
