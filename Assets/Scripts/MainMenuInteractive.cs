﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuInteractive : MonoBehaviour {
	//Clica no play
	//Desativa o IsKinematic do Play
	//Listen to Play hit transition trigger area.
	//When Trigger Hit reset Play to start position
	//Show Stage Select Panel

	public Rigidbody playButton;
	public Rigidbody settingsButton;
	public Rigidbody aboutButton;
	public Rigidbody statsButton;

	public GameObject playPanel;
	public GameObject settingsPanel;
	public GameObject aboutPanel;
	public GameObject statsPanel;

	private InteractiveButton ibPlay;
	private InteractiveButton ibSettings;
	private InteractiveButton ibAbout;
	private InteractiveButton ibStats;

	void Awake(){
		Time.timeScale = 1;
		ibPlay = playButton.GetComponent<InteractiveButton> ();
		ibSettings = settingsButton.GetComponent<InteractiveButton> ();
		ibAbout = aboutButton.GetComponent<InteractiveButton> ();
		ibStats = statsButton.GetComponent<InteractiveButton> ();

	}

	void OnTriggerEnter(Collider other){
		Debug.Log (other.transform.name);
		switch(other.transform.name){
		case "Play":
			//show stage select panel
			playPanel.SetActive (true);
			ibPlay.Reset ();
			break;
		case "Settings":
			//show settings panel
			settingsPanel.SetActive (true);
			ibSettings.Reset ();
			break;
		case "About":
			//show about panel
			aboutPanel.SetActive (true);
			ibAbout.Reset ();
			break;
		case "Stats":
			//show stats panel
			statsPanel.SetActive (true);
			ibStats.Reset ();
			break;
		}
	}

	public void Play(){
		ibPlay.Go ();
	}

	public void Settings(){
		ibSettings.Go();
	}

	public void Stats(){
		ibStats.Go();
	}

	public void About(){
		ibAbout.Go();
	}
}
