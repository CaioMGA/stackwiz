﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagesInfos : MonoBehaviour {
	public float[] w1_times;
	public float[] w2_times;
	public bool DELETEALLINFO = false;

	string lang;
	void Start(){
		if(!PlayerPrefs.HasKey("FirstRun") || DELETEALLINFO){
			FirstRun ();
		}
	}

	void FirstRun(){
		// sets the stages informations and resets any existing progress
		if(PlayerPrefs.HasKey("curLang")){
			lang = PlayerPrefs.GetString ("curLang");
		} else {
			lang = "en";
		}

		PlayerPrefs.DeleteAll ();

		initWorld (1, w1_times);
		initWorld (2, w2_times);

		PlayerPrefs.SetInt ("1-1_Locked", 0);
		PlayerPrefs.SetInt ("1-2_Locked", 0);
		PlayerPrefs.SetInt ("lastUnlockedStage", 2);
		PlayerPrefs.SetInt ("lastUnlockedWorld", 1);
		PlayerPrefs.SetInt ("FirstRun", 1);
		PlayerPrefs.SetInt ("ShowGrid", 1);
		PlayerPrefs.SetString("curLang", lang);

	}

	void initWorld(int worldNum, float[] info){
		int stages = info.Length;
		for(int i = 0; i < stages; i++){
			int stageNum = i + 1;
			PlayerPrefs.SetFloat (worldNum + "-" + stageNum + "_Extra_Star_Time", info[i]);
			PlayerPrefs.SetFloat (worldNum + "-" + stageNum + "_Personal_Best_Time", 0);
			PlayerPrefs.SetInt (worldNum + "-" + stageNum + "_Stars", 0);
			//PlayerPrefs.SetInt (worldNum + "-" + stageNum + "_Locked", 1);

			PlayerPrefs.Save ();
		}
		Debug.Log ("World " + worldNum + " Info created!");
	}

	public void RESETDATA(){
		FirstRun ();
	}
}
