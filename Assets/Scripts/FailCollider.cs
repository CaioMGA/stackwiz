﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailCollider : MonoBehaviour {

	GameLogic _gl;
	SpeedRunLogic _srl;

	void Start(){
		_gl = GameObject.FindObjectOfType<GameLogic> ();
		_srl = GameObject.FindObjectOfType<SpeedRunLogic> ();

	}

	void OnCollisionEnter(Collision other){
		if(_srl == null){
			_gl.Fail ();
		} else {
			_srl.Fail ();
		}
	}
}
