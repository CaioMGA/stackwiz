﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {
	[Header("Objects")]
	public Sprite[] DropThumbs;
	public int[] DropOrder; //order of objects to be stacked
	[Space]
	[Header("Menu")]
	public GameObject PauseButton;
	public GameObject YouLosePanel;
	public GameObject YouWinPanel;
	public RectTransform queueContent;
	public GameObject queueButton;
	public GameObject frameNextPiece;
	public EventSystem _eventSystem;
	[Space]
	public GameObject starsUI;
	public GameObject extraStar;
	public float extraStarTime;
	public GameObject clockUI;
	public Text DropTimer;
	public Text BestTime;
	public Text BestTimeYouWin;
	public Text ExtraStarTimeUI;
	public Animator extraStarGoalInGame;
	[Space]
	[Header("Audio")]
	public AudioClip PieceDrop;
	public GameObject _musicPlayer;
	[Header("Aim")]
	public GameObject aim;
	[Space]
	public GameObject ghost;
	[Header("Ghost Pieces")]
	public GameObject cube;
	public GameObject cube45;
	public GameObject rectangle;
	public GameObject rectangle90;
	public GameObject triangleUp;
	public GameObject triangleDown;
	public GameObject triangleJ;
	public GameObject triangleL;
	public GameObject sphere;
	public GameObject smallRectangle90;
	public GameObject pato;
	public GameObject porco;
	[Space]
	[Header("Replay")]
	public GameObject replays;
	public GameObject replayIsDonePanel;
	public Material replaySkybox;
	Material levelSkybox;


	Ad _ad;
	AimAssistSettings aimAssistSettings;
	AudioSource _audioSource;
	GameObject dropHolder;
	GridScript _grid;
	List<Vector3> DropPos;
	List<float> DropDelayCount;
	PlaceObject _placeObj;
	[HideInInspector]
	public PlayerMoveMonitor playerMoveMonitor; //set by monitor itself
	QueueCounter queueCounter;
	Replay replay;
	UnlockStage unlockStage;
	TextHandler textHandler;
	StatsAux statsAux;

	string stageName;
	//string aimAssistSettings.aimAssistMode;

	float clockShowTime = 12; //tempo em que o relógio fica na tela
	float maxWinTime = 10;
	float minWinTime = 5;
	float winTimeDeltatime = 0;
	float dropTime = 0;
	float DropDelay = 2;
	float f_bestTime = 0;
	float dropTimeUpdateDelay = 1;
	float dropTimeUpdateDelayDeltatime = 0;
	//int unpauseDelay = 2;
	//int unpauseCounter = 0;
	int nextPieceDrop;
	int piecesCount;

	string gameState = "init";
	bool winTimerActive = false;
	bool gameover = false;
	bool countingDropTime = false;
	bool paused = false;
	bool unpausing = false;
	bool showingReplay = false;
	bool showingAim = false;

	//HACKS
	bool HACK_mouseDown = false;
	bool HACK_mouseUp = false;

	public void Init(){
		dropHolder = new GameObject ();
		levelSkybox = RenderSettings.skybox;
		_grid = GameObject.FindObjectOfType<GridScript> ();
		stageName = SceneManager.GetActiveScene ().name;
		_placeObj = GetComponent<PlaceObject> ();
		textHandler = GameObject.FindObjectOfType<TextHandler> ();
		//InitDropList ();
		piecesCount = DropOrder.Length;
		nextPieceDrop = 0;
		InitQueue ();
		unlockStage = new UnlockStage ();
		_audioSource = GetComponent<AudioSource> ();
		if(!FindMusicPlayer()){
			Instantiate (_musicPlayer);
		}
		Time.timeScale = 1;
		DropPos = new List<Vector3>();
		DropDelayCount = new List<float>();
		float f_extraStar = PlayerPrefs.GetFloat (stageName + "_Extra_Star_Time");
		ExtraStarTimeUI.text = f_extraStar.ToString("N3")+ " " + textHandler.GetUnitSecond();
		f_bestTime = PlayerPrefs.GetFloat (stageName + "_Personal_Best_Time");
		BestTime.text = f_bestTime.ToString("N3")+ " " + textHandler.GetUnitSecond();
		if(f_bestTime < f_extraStar && f_bestTime > 0){
			extraStarGoalInGame.GetComponentInChildren<Text>().text = "Best: " + f_bestTime.ToString("N3")+ " " + textHandler.GetUnitSecond();
		} else {
			extraStarGoalInGame.GetComponentInChildren<Text>().text = "+1★: " + f_extraStar.ToString("N3")+ " " + textHandler.GetUnitSecond();
		}
		aimAssistSettings = GetComponent<AimAssistSettings> ();
		aimAssistSettings.aimAssistMode = aimAssistSettings.GetAssistMode ();
		aimAssistSettings.UpdateAssistModeButtons ();
		queueCounter = GetComponent<QueueCounter> ();
		queueCounter.init(DropOrder.Length);
		statsAux = GetComponent<StatsAux> ();
		Utils.Stats.AddTry ();
		Debug.Log ("Init successful");
		gameState = "game";
	}

	void FixedUpdate(){
		if(gameState.Equals("init")){
			Init ();
		}
		if (gameState.Equals ("game")) {
			UpdateStagedCounters ();
			AimAssists ();
			if (_eventSystem.IsPointerOverGameObject ()) {
				//verifica se está sobre um elemento da UI
			} else if (nextPieceDrop < piecesCount) {
				if (Input.GetMouseButtonDown (0) && !HACK_mouseDown) {
					HACK_mouseDown = true;
					HACK_mouseUp = false;
					ShowAim (true, Input.mousePosition);
					Debug.Log ("Mouse DOWN");

				}
				if (Input.GetMouseButtonUp (0) && !HACK_mouseUp && HACK_mouseDown) {
					HACK_mouseDown = false;
					HACK_mouseUp = true;
					StageDrop (Input.mousePosition);
					ShowAim (false, Input.mousePosition);
					Debug.Log ("Mouse UP");
				}
			} else if (!winTimerActive) {
				StartWinTimer ();
			}
			if (countingDropTime) {
				dropTime += Time.deltaTime;
				UpdateDropTimerDisplay (false);
			}

			if (winTimerActive) {
				if (winTimeDeltatime < maxWinTime) {
					winTimeDeltatime += Time.deltaTime;
				} else {
					if (winTimeDeltatime < clockShowTime) {
						winTimeDeltatime += Time.deltaTime;
					} else {
						Win ();
						gameState = "win";
					}
				}
			}
		} else if (gameState.Equals ("replay")) {
			UpdateStagedCounters ();
			Debug.Log ("ShowingSolution");
			replay.Tick ();
			if (replay.done) {
				replayIsDonePanel.SetActive (true);
			}
		} else if (gameState.Equals ("pause")) {
			
		} else if (gameState.Equals ("unpausing")) {
			
		}
	}
	void StageDrop(Vector3 _pos){
		DropDelayCount.Add (0);
		DropPos.Add (_pos);
	}

	void UpdateStagedCounters(){
		int stagedCounters = DropDelayCount.Count;
		bool[] deleteCounters = new bool[stagedCounters];
		for(int i = 0; i < stagedCounters; i++){
			deleteCounters [i] = false;
			DropDelayCount [i] += 1;
			if(DropDelayCount [i] >= DropDelay){
				Drop (DropPos [0]);
				DropPos.RemoveAt (0);
				//DropDelayCount.RemoveAt (0);
				deleteCounters [i] = true;
				//Debug.Log ("delete");
			}
		}
		for(int j = 0; j < stagedCounters; j++){
			if(deleteCounters[j]){
				if(DropDelayCount.Count > 0){
					DropDelayCount.RemoveAt (0);
				} else {
					return;
				}
			}
		}
	}

	void Drop(Vector3 mousePos){
		if(nextPieceDrop < piecesCount && !winTimerActive){
			if(_placeObj.Drop (DropOrder[nextPieceDrop], mousePos, false, dropHolder)){
				statsAux.AddDrop ();//contador de peças jogadas
				_audioSource.pitch *= 1.05f;
				_audioSource.PlayOneShot (PieceDrop);

				UpdatePlayerMovesMonitor (DropOrder[nextPieceDrop], mousePos, dropTime);
				//PlayerMoveMonitor
				//Update replay 

				nextPieceDrop++;
				queueCounter.UpdateCounter ();
				if(queueContent.childCount > 1){
					queueContent.transform.GetChild (0).transform.GetChild (0).transform.SetParent(queueContent.transform.GetChild (1).transform);
					FrameUpdate (queueContent.transform.GetChild (1).transform.GetChild (0));
				}
				queueContent.sizeDelta = new Vector2 (((piecesCount - nextPieceDrop) * 160), 0);
				if(!countingDropTime){
					countingDropTime = true;
					extraStarGoalInGame.SetTrigger ("FADEOUT");
				}
				Destroy(queueContent.transform.GetChild (0).gameObject);
			}
		} else {
			StartWinTimer ();
		}
	}

	void StartWinTimer(){
		statsAux.saveDrops ();
		countingDropTime = false;
		UpdateDropTimerDisplay (true);
		Debug.Log ("Drop Time:" + dropTime);
		winTimerActive = true;
		winTimeDeltatime = 0;
		queueContent.parent.parent.gameObject.SetActive (false);
		queueButton.SetActive (false);
		clockUI.SetActive (true);
		PauseButton.SetActive (false);
	}

	int CalculateStars(){
		int stars = 0;
		winTimerActive = false;

		if(winTimeDeltatime >= maxWinTime){
			stars += 2;
		} else if(winTimeDeltatime >= minWinTime){
			stars += 1;
		}
		if(dropTime < extraStarTime){
			stars += 1;
			extraStar.SetActive (true);
		}
		float curBestTime = PlayerPrefs.GetFloat (stageName + "_Personal_Best_Time");
		if(curBestTime > 0){
			if(dropTime < curBestTime){
				PlayerPrefs.SetFloat (stageName + "_Personal_Best_Time", dropTime);
			}
		} else {
			PlayerPrefs.SetFloat (stageName + "_Personal_Best_Time", dropTime);
		}

		return stars;
	}

	void ShowStars (int stars){
		for(int i=0; i<stars; i++){
			starsUI.transform.GetChild(i).gameObject.SetActive(true);
			starsUI.transform.GetChild(i+3).gameObject.SetActive(false);
		}
	}

	public void Fail(){
		ShowAim (false, Vector3.zero);
		if(!gameover){
			if(winTimeDeltatime >= minWinTime){
				Win ();
			} else {
				gameState = "lose";
				//show you lose menu panel
				statsAux.saveDrops ();
				YouLosePanel.SetActive (true);
				PauseButton.SetActive (false);
				gameover = true;
				queueContent.parent.parent.gameObject.SetActive (false);
				queueButton.SetActive (false);
				clockUI.SetActive (false);
			}
		}
	}

	void Win(){
		if (!gameover) {
			gameState = "win";
			Utils.Stats.SetStageClear (stageName);
			ShowAim (false, Vector3.zero);
			unlockStage.Unlock (stageName);
			int starsTotal = CalculateStars ();
			ShowStars (starsTotal);
			Utils.Stats.UpdateStars (stageName, starsTotal);
			BestTimeYouWin.text = PlayerPrefs.GetFloat (stageName + "_Personal_Best_Time").ToString("N3")+ " " + textHandler.GetUnitSecond();
			YouWinPanel.SetActive (true);
			PauseButton.SetActive (false);
			clockUI.SetActive (false);
			gameover = true;
		}
	}

	void InitQueue (){
		int drops = DropOrder.Length;
		//queueContent.rect = new Rect (queueContent.rect.x, queueContent.rect.y, queueContent.rect.width, queueContent.rect.height);
		queueContent.sizeDelta = new Vector2 ((drops * 160), 0);
		for(int i = 0; i < drops; i++){
			GameObject thumb = new GameObject ("img");
			thumb.transform.parent = queueContent.transform;
			thumb.AddComponent<Image> ();
			thumb.GetComponent<Image> ().sprite = DropThumbs [DropOrder [i]];
			thumb.transform.localScale = new Vector2 (1, 1);
			thumb.GetComponent<Image> ().preserveAspect = true;
		}


		GameObject nextFrame = Instantiate(frameNextPiece);
		nextFrame.transform.parent = queueContent.GetChild (0).transform;
		FrameUpdate (nextFrame.transform);

	}

	void FrameUpdate(Transform frame){
		frame.transform.localScale = new Vector2 (1, 1);
		frame.GetComponent<RectTransform> ().anchoredPosition = new Vector2(0, 0);
		frame.GetComponent<RectTransform> ().anchorMax = new Vector2 (1, 1);
		frame.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 0);
		frame.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.5f);
		frame.GetComponent<Image> ().preserveAspect = true;
		frame.GetComponent<RectTransform> ().sizeDelta = new Vector2(1, 1);
	}

	public void Pause(){
		Time.timeScale = 0;
		paused = true;
		ShowAim (false, Vector3.zero);

		DropDelayCount.Clear();
		DropPos.Clear();
		gameState = "pause";
		Debug.Log ("Timescale = 0");
	}

	public void Unpause(){
		paused = false;
		unpausing = true;
		gameState = "unpausing";
		DropDelayCount.Clear();
		DropPos.Clear();
		Time.timeScale = 1;
		unpausing = false;
		gameState = "game";
		Debug.Log ("Timescale = 1");
	}

	bool FindMusicPlayer(){
		if(GameObject.FindObjectsOfType<MusicPlayer>().Length < 1 ){
			return false;
		}
		return true;
	}

	void UpdateDropTimerDisplay(bool ignoreDelay){
		string dropTimeString = dropTime.ToString("N3");
		if(ignoreDelay){
			DropTimer.text = dropTimeString + " " + textHandler.GetUnitSecond();
		} else if(dropTimeUpdateDelayDeltatime > dropTimeUpdateDelay){
			DropTimer.text = dropTimeString + " " + textHandler.GetUnitSecond();
			dropTimeUpdateDelayDeltatime = 0;
		} else {
			dropTimeUpdateDelayDeltatime += 1;
		}

	}

	void TrackTouch (){
		if(aimAssistSettings.aimAssistMode.Equals("Red Cross")){
			aim.transform.position = Input.mousePosition;
		} else if(aimAssistSettings.aimAssistMode.Equals("Ghost Pieces")){
			_placeObj.MoveObjOnPlane(ghost, Input.mousePosition);
		} else {
			//No Assist
		}
	}
	void ShowAim (bool val, Vector3 mousePos){
		showingAim = val;
		if(aimAssistSettings.aimAssistMode.Equals("Red Cross")){
			TrackTouch ();
			aim.SetActive (val);
		} else if(aimAssistSettings.aimAssistMode.Equals("Ghost Pieces")){
			if(val){
				ghost = GetGhostPiece (mousePos);
				TrackTouch ();
			} else {
				Destroy (ghost);
			}
		} else {
			//No Assist
		}
	}

	void AimAssists(){
		if(aimAssistSettings.aimAssistMode.Equals("Red Cross")){
			if(showingAim){
				TrackTouch ();
			}
		} else if(aimAssistSettings.aimAssistMode.Equals("Ghost Pieces")){
			if(showingAim){
				TrackTouch ();
			}
		}
	}

	GameObject GetGhostPiece (Vector3 mousePos){
		string nextPiecetag;
		GameObject piece = null;
		if(nextPieceDrop >= piecesCount){
			return null;
		}
		nextPiecetag = _placeObj.GetPieceTag(DropOrder[nextPieceDrop]);
		switch (nextPiecetag){
		case "cube45":
			piece = Instantiate (cube45);
			break;
		case "rectangle":
			piece = Instantiate (rectangle);
			break;
		case "rectangle90":
			piece = Instantiate (rectangle90);
			break;
		case "rectangleSmall90":
			piece = Instantiate (smallRectangle90);
			break;
		case "triangleUp":
			piece = Instantiate (triangleUp);
			break;
		case "triangleDown":
			piece = Instantiate (triangleDown);
			break;
		case "triangleL":
			piece = Instantiate (triangleL);
			break;
		case "triangleJ":
			piece = Instantiate (triangleJ);
			break;
		case "sphere":
			piece = Instantiate (sphere);
			break;
		case "pato":
			piece = Instantiate (pato);
			break;
		case "porco":
			piece = Instantiate (porco);
			break;
		default:
			piece = Instantiate (cube);
			break;
		}
		_placeObj.MoveObjOnPlane (piece, mousePos);
		return piece;
	}

	void UpdatePlayerMovesMonitor (int type, Vector3 pos, float _dropTime){
		if(playerMoveMonitor != null){
			playerMoveMonitor.newMove (type, pos, _dropTime);
		}
	}

	public void StartSolution(){
		showingReplay = true;
		replay = replays.transform.GetChild(GetSolutionIndex ()).GetComponent<Replay>();
		_grid.SetGridReplay (true);
		RenderSettings.skybox = replaySkybox;
		Unpause ();
		replay.Play ();
		Destroy (dropHolder);
	}

	public void RestartSolution(){
		replay.Restart ();
	}

	public void ExitSolution(){
		showingReplay = false;
		_grid.SetGridReplay (false);
		RenderSettings.skybox = levelSkybox;
		replayIsDonePanel.SetActive (false);
		replay.Clear ();
	}

	int GetSolutionIndex(){
		string sceneName = SceneManager.GetActiveScene ().name;
		int solutionIndex = int.Parse(stageName.Substring (2, stageName.Length - 2));
		solutionIndex--;
		return solutionIndex;
	}

}
