﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMove : System.Object{
	public int pieceType;
	public Vector3 dropPosition;
	public float dropTime;

	public PlayerMove(int _pieceType, Vector3 _dropPosition, float _dropTime){
		pieceType = _pieceType;
		dropPosition = _dropPosition;
		dropTime = _dropTime;
	}
	public int GetPieceType(){
		return pieceType;
	}
	public Vector3 GetPosition(){
		return dropPosition;
	}
	public float GetDropTime(){
		return dropTime;
	}
	public void PrintPlayerMove(){
		Debug.Log ("type: " + GetPieceType () + " Position: " + GetPosition() + " Time: " + GetDropTime()) ;
	}

}
