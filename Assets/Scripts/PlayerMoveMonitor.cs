﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMoveMonitor : MonoBehaviour {
	//public ArrayList moves = new ArrayList();
	GameObject go;
	GameObject SOLUTIONS;

	int curPiece = 0;

	void Start(){
		DontDestroyOnLoad (this.gameObject);
		SOLUTIONS = new GameObject ();
		SOLUTIONS.transform.parent = transform;
		SOLUTIONS.name = "SOLUTIONS";
	}

	public void CreateNewReplay(){
		GameLogic gl = GameObject.FindObjectOfType<GameLogic> ();
		gl.playerMoveMonitor = this;
		go = new GameObject();
		go.transform.parent = SOLUTIONS.transform;
		go.name = SceneManager.GetActiveScene ().name;
		go.AddComponent<Replay> ();
		go.GetComponent<Replay> ().init (GameObject.FindObjectOfType<PlaceObject> (), gl.DropOrder.Length);
		curPiece = 0;

	}

	public void newMove(int type, Vector3 pos, float dropTime){
		go.GetComponent<Replay>().array_moves[curPiece] = (new PlayerMove (type, PlaceObject.GetWorldPosition(pos), dropTime));
		curPiece++;
	}

	public void PlayMoves (){
		Replay replay = go.GetComponent<Replay> ();
		foreach(var playerMove in replay.array_moves){
			playerMove.PrintPlayerMove ();
		}
	}

}
