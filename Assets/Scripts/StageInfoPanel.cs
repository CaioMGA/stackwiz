﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageInfoPanel : MonoBehaviour {
	public GameObject panel;
	[Space]
	public Text UI_WorldNum;
	public Text UI_StageNum;
	public GameObject UI_Stars;
	public Text UI_PersonalBestTime;
	public Text UI_ExtraStarTime;
	public Button PlayButton;

	TextHandler textHandler;

	string stageName;
	string worldNum;
	string stageNum;
	float personalBestTime;
	float extraStarTime;
	int starsReached;

	void Start(){
		textHandler = GameObject.FindObjectOfType<TextHandler> ();
	}

	public void ShowStageInfo(string _stageName){ // chamado pelo botao de fase
		LoadInfo (_stageName);
		UpdateUI ();
		ShowPanel ();
	}

	public void Play(){
		SceneManager.LoadScene (stageName);
	}

	public void Next(){
		int nextStage = (int.Parse (stageNum) + 1);
		int nextWorld = int.Parse (worldNum);

		if(nextStage > 20){
			if(nextWorld < 6){
				nextWorld += 1;
				nextStage = 1;
			} else {
				return;
			}
		}
		string nextStageName = nextWorld.ToString() + "-" + nextStage.ToString();
		UI_Stars.SetActive (false);
		ShowStageInfo (nextStageName);
		UI_Stars.SetActive (true);
	}

	public void Prev(){
		int prevStage = (int.Parse (stageNum) - 1);
		int prevWorld = int.Parse (worldNum);
		if(prevStage < 1){
			if(prevWorld > 1){
				prevWorld -= 1;
				prevStage = 20;
			} else {
				return;
			}
		}
		string prevStageName = prevWorld.ToString() + "-" + prevStage.ToString();
		UI_Stars.SetActive (false);
		ShowStageInfo (prevStageName);
		UI_Stars.SetActive (true);
	}

	void LoadInfo(string _stageName){
		stageName = _stageName;
		string [] values = stageName.Split ('-');
		worldNum = values [0];
		stageNum = values [1];
		personalBestTime = PlayerPrefs.GetFloat (stageName + "_Personal_Best_Time");
		extraStarTime = PlayerPrefs.GetFloat (stageName + "_Extra_Star_Time");
		starsReached = PlayerPrefs.GetInt (stageName + "_Stars");

	}

	void UpdateUI(){
		HandlesPlayButton ();
		UI_WorldNum.text = worldNum;
		UI_StageNum.text = stageNum;
		UI_PersonalBestTime.text = personalBestTime.ToString ("N3") + " " + textHandler.GetUnitSecond();
		UI_ExtraStarTime.text = extraStarTime.ToString ("N3") + " " + textHandler.GetUnitSecond();
		ShowStars ();

	}

	void ShowPanel(){
		panel.SetActive (true);
	}

	void ShowStars (){
		for(int i=0; i<starsReached; i++){
			UI_Stars.transform.GetChild(i).gameObject.SetActive(true);
			UI_Stars.transform.GetChild(i+3).gameObject.SetActive(false);
		}

		for(int i = starsReached; i<3;i++){
			UI_Stars.transform.GetChild(i).gameObject.SetActive(false);
			UI_Stars.transform.GetChild(i+3).gameObject.SetActive(true);
		}
	}

	void HandlesPlayButton (){
		bool stage_lock = PlayerPrefs.HasKey(stageName + "_Locked");
		PlayButton.interactable = stage_lock;
	}
}
