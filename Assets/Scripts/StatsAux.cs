﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class StatsAux : MonoBehaviour {

	int drops = 0;

	public void AddDrop(){
		drops += 1;
	}

	public void saveDrops(){
		Utils.Stats.SaveDrops (drops);
		drops = 0;
	}
}
