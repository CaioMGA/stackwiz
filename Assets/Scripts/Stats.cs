﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils{
	public class Stats {

		public static int GetDrops(){
			return PlayerPrefs.GetInt ("DROPS");
		}

		public static int GetStars(){
			return PlayerPrefs.GetInt ("Stars_Total");
		}

		public static int GetStagesCleared(){
			return PlayerPrefs.GetInt ("STAGESCLEARED");
		}

		public static int GetTries(){
			return PlayerPrefs.GetInt ("TRIES");
		}

		public static void SaveDrops(int drops){
			drops += PlayerPrefs.GetInt ("DROPS");
			PlayerPrefs.SetInt ("DROPS", drops);
			Debug.Log ("New Drop Count: " + drops);
		}

		public static void UpdateStars(string stageName, int starsEarned){
			int currentStars = PlayerPrefs.GetInt (stageName + "_Stars");
			if(starsEarned > currentStars){
				int starsTotal = PlayerPrefs.GetInt ("Stars_Total");
				starsTotal -= currentStars;
				starsTotal += starsEarned;
				PlayerPrefs.SetInt (stageName + "_Stars", currentStars);
				PlayerPrefs.SetInt ("Stars_Total", starsTotal);
			}
		}

		public static void SetStageClear(string stageName){
			if (!PlayerPrefs.HasKey (stageName + "_Cleared")){
				PlayerPrefs.SetInt (stageName + "_Cleared", 0);
				int stagesCleared = PlayerPrefs.GetInt ("STAGESCLEARED");
				stagesCleared += 1;
				PlayerPrefs.SetInt ("STAGESCLEARED", stagesCleared);
			}

		}

		public static void AddTry(){
			int tries = GetTries () + 1;
			PlayerPrefs.SetInt ("TRIES", tries);
		}
	}
}

