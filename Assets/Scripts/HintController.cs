﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HintController : MonoBehaviour {
	public GameObject buySolutionPanel;
	public GameObject watchSolutionPanel;
	public Animator hintBtnAnimator;

	public void UnlockHint(){
		PlayerPrefs.SetInt (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name +"_clue", 0);
	}

	bool isSolutionLocked(){
		if(PlayerPrefs.HasKey(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name +"_clue")){
			return true;
		}
		return false;
	}

	public void SolutionButtonPressed(){
		//GameObject.FindObjectOfType<GameLogic> ().Unpause ();
		if(isSolutionLocked()){
			buySolutionPanel.SetActive (false);
			watchSolutionPanel.SetActive (true);
		} else {
			buySolutionPanel.SetActive (true);
			watchSolutionPanel.SetActive (false);
		}
	}

	public void animateHintButton(){
		hintBtnAnimator.SetTrigger ("Animate");
	}

	public void setHintButtonToIdle(){
		hintBtnAnimator.SetTrigger ("Idle");
	}

	public void ShowRewardedVideo(){
		GameObject.FindObjectOfType<Ad> ().ShowRewardedVideo ();
	}
}
