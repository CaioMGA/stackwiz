﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockStage : MonoBehaviour {
	public GameObject[] worlds;
	public Button[] stageBtns;
	void Start(){
		UpdateStageButtons ();
	}

	public void Unlock(string stageName){
		int world = int.Parse(stageName.Substring (0, 1));
		int stage = int.Parse(stageName.Substring (2, stageName.Length - 2));

		stage += 2;
		if(stage > 20){
			stage = 1;
			world += 1;
		}

		if(!PlayerPrefs.HasKey (world + "-" + stage + "_Locked")){
			PlayerPrefs.SetInt (world + "-" + stage + "_Locked", 0);
			PlayerPrefs.SetInt ("lastUnlockedStage", stage);
			PlayerPrefs.SetInt ("lastUnlockedWorld", world);
			Debug.Log ("Stage unlocked: " + world + "-" + stage);
		}
	}
	/*
	public void UnlockNextStage(){
		int lastStageUnlocked = PlayerPrefs.GetInt ("lastUnlockedStage");
		int lastWorldUnlocked = PlayerPrefs.GetInt ("lastUnlockedWorld");

		if (lastStageUnlocked >= 20) {
			lastWorldUnlocked += 1;
			lastStageUnlocked = 1;
		} else {
			lastStageUnlocked += 1;
		}

		PlayerPrefs.SetInt (lastWorldUnlocked + "-" + lastStageUnlocked + "_Locked", 0);
		PlayerPrefs.SetInt ("lastUnlockedStage", lastStageUnlocked);
		PlayerPrefs.SetInt ("lastUnlockedWorld", lastWorldUnlocked);
	}
	*/

	public void UpdateStageButtons(){
		Debug.Log ("Stage Buttons Updated");
		int lastStage = PlayerPrefs.GetInt ("lastUnlockedStage");
		int lastWorld = PlayerPrefs.GetInt ("lastUnlockedWorld");

		for(int w = 1; w <= lastWorld; w++){
			//Button [] stageBtns = worlds[w-1].GetComponentsInChildren<Button>();
			stageBtns = worlds[w-1].GetComponentsInChildren<Button>();
			for(int s = 0; s < 20 ; s ++){
				int buttonNameToInt = int.Parse (stageBtns[s].name);
				if(w < lastWorld){
					if(buttonNameToInt <= 20){
						stageBtns [s].interactable = true;
					}
				} else {
					if(buttonNameToInt <= lastStage){
						stageBtns [s].interactable = true;
					} else {
						stageBtns [s].interactable = false;
					}
				}

			}
		}
	}
}