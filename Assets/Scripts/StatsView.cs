﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class StatsView : MonoBehaviour {
	public Text drops;
	public Text stars;
	public Text stagesCleared;
	public Text tries;

	void Awake(){
		drops.text = Utils.Stats.GetDrops ().ToString();
		stars.text = Utils.Stats.GetStars ().ToString();
		stagesCleared.text = Utils.Stats.GetStagesCleared ().ToString();
		tries.text = Utils.Stats.GetTries ().ToString();
	}
}
