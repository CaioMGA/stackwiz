﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextHandler : MonoBehaviour {
	public Text [] mu_back;
	public Text [] mu_play;
	public Text [] mu_settings;
	public Text [] mu_main_menu;
	public Text [] mu_retry;
	public Text [] mu_show_grid;
	public Text mm_title;
	public Text mm_author_name;
	public Text si_title;
	public Text si_world_num;
	public Text si_stage_num;
	public Text si_extra_star;
	public Text si_best_time;
	public Text stt_music;
	public Text stt_sound;
	public Text stt_languages;
	public Text stt_aim_assists;
	public Text stt_aa_red_cross;
	public Text stt_aa_ghost_pieces;
	public Text stt_aa_no_assist;
	public Text stt_reset_all_progress;
	public Text ss_title;
	public Text ss_worlds;
	public Text ss_stages;
	public Text ss_loading;
	public Text sts_title;
	public Text sts_pieces_droped;
	public Text sts_stars_earned;
	public Text sts_stages_completed;
	public Text sts_stages_failed;
	public Text sts_reset_ALL_progress;
	//public Text sts_stages_total_completion_time;
	//public Text sts_stages_avegare_completion_time;
	public Text ig_queue;
	public Text ig_next;
	public Text ig_hint;
	public Text igp_title;
	public Text igp_best_time_lbl;
	public Text igp_extra_star_time_lbl;
	public Text igp_resume;
	public Text igp_restart;
	public Text yl_title;
	public Text yw_title;
	public Text yw_best_time_lbl;
	public Text yw_next;
	public Text bs_msg;
	public Text ws_title;
	public Text ws_watch;
	public Text sr_post_wave_title;

	LanguageSelect lang;
	TextAsset textData;

	void Awake(){
		lang = GameObject.FindObjectOfType<LanguageSelect> ();
		lang.GetCurLang ();
		textData = lang.GetTextAsset ();
		UpdateText ();
	}

	void UpdateText(){
		if(mu_back.Length > 0){
			for(int i = 0; i < mu_back.Length; i++){
				if(mu_back[i] != null){
					mu_back[i].text = textData.mu_back;
				}
			}			
		}
		if(mu_play.Length > 0){
			for(int i = 0; i < mu_play.Length; i++){
				if(mu_play[i] != null){
					mu_play[i].text = textData.mu_play;
				}
			}			
		}
		if(mu_settings.Length > 0){
			for(int i = 0; i < mu_settings.Length; i++){
				if(mu_settings[i] != null){
					mu_settings[i].text = textData.mu_settings;
				}
			}			
		}
		if(mu_main_menu.Length > 0){
			for(int i = 0; i < mu_main_menu.Length; i++){
				if(mu_main_menu[i] != null){
					mu_main_menu[i].text = textData.mu_main_menu;
				}
			}			
		}
		if(mu_retry.Length > 0){
			for(int i = 0; i < mu_retry.Length; i++){
				if(mu_retry[i] != null){
					mu_retry[i].text = textData.mu_retry;
				}
			}			
		}
		if(mu_show_grid.Length > 0){
			for(int i = 0; i < mu_show_grid.Length; i++){
				if(mu_show_grid[i] != null){
					mu_show_grid[i].text = textData.mu_show_grid;
				}
			}			
		}
		if(mm_title != null){
			mm_title.text = textData.mm_title;
		}
		if(mm_author_name != null){
			mm_author_name.text = textData.mm_author_name;
		}
		if(si_title != null){
			si_title.text = textData.si_title;
		}
		if(si_world_num != null){
			si_world_num.text = textData.si_world_num;
		}
		if(si_stage_num != null){
			si_stage_num.text = textData.si_stage_num;
		}
		if(si_extra_star != null){
			si_extra_star.text = textData.si_extra_star;
		}
		if(si_best_time != null){
			si_best_time.text = textData.si_best_time;
		}
		if(stt_music != null){
			stt_music.text = textData.stt_music;
		}
		if(stt_sound != null){
			stt_sound.text = textData.stt_sound;
		}
		if(stt_languages != null){
			stt_languages.text = textData.stt_languages;
		}
		if(stt_aim_assists != null){
			stt_aim_assists.text = textData.stt_aim_assists;
		}
		if(stt_aa_red_cross != null){
			stt_aa_red_cross.text = textData.stt_aa_red_cross;
		}
		if(stt_aa_ghost_pieces != null){
			stt_aa_ghost_pieces.text = textData.stt_aa_ghost_pieces;
		}
		if(stt_aa_no_assist != null){
			stt_aa_no_assist.text = textData.stt_aa_no_assist;
		}
		if(stt_reset_all_progress != null){
			stt_reset_all_progress.text = textData.stt_reset_all_progress;
		}
		if(ss_title != null){
			ss_title.text = textData.ss_title;
		}
		if(ss_worlds!= null){
			ss_worlds.text = textData.ss_worlds;
		}
		if(ss_stages != null){
			ss_stages.text = textData.ss_stages;
		}
		if(ss_loading != null){
			ss_loading.text = textData.ss_loading;
		}
		if(sts_title != null){
			sts_title.text = textData.sts_title;
		}
		if(sts_pieces_droped != null){
			sts_pieces_droped.text = textData.sts_pieces_droped;
		}
		if(sts_stars_earned != null){
			sts_stars_earned.text = textData.sts_stars_earned;
		}
		if(sts_stages_completed != null){
			sts_stages_completed.text = textData.sts_stages_completed;
		}
		if(sts_stages_failed != null){
			sts_stages_failed.text = textData.sts_stages_failed;
		}
		if(sts_reset_ALL_progress != null){
			sts_reset_ALL_progress.text = textData.sts_reset_ALL_progress;
		}
		if(ig_queue != null){
			ig_queue.text = textData.ig_queue;
		}
		if(ig_next != null){
			ig_next.text = textData.ig_next;
		}
		if(ig_hint != null){
			ig_hint.text = textData.ig_hint;
		}
		if(igp_title != null){
			igp_title.text = textData.igp_title;
		}
		if(igp_extra_star_time_lbl != null){
			igp_extra_star_time_lbl.text = textData.igp_extra_star_time_lbl;
		}
		if(igp_best_time_lbl != null){
			igp_best_time_lbl.text = textData.igp_best_time_lbl;
		}
		if(igp_resume != null){
			igp_resume.text = textData.igp_resume;
		}
		if(igp_restart != null){
			igp_restart.text = textData.igp_restart;
		}
		if(yl_title != null){
			yl_title.text = textData.yl_title;
		}
		if(yw_title != null){
			yw_title.text = textData.yw_title;
		}
		if(yw_best_time_lbl != null){
			yw_best_time_lbl.text = textData.yw_best_time_lbl;
		}
		if(yw_next != null){
			yw_next.text = textData.yw_next;
		}
		if(bs_msg != null){
			bs_msg.text = textData.bs_msg;
		}
		if(ws_title != null){
			ws_title.text = textData.ws_title;
		}
		if(ws_watch != null){
			ws_watch.text = textData.ws_watch;
		}

		//UpdatePostWaveTitle (0);//just init it
	}

	public void ChangeLang(string newLang){
		lang.SetCurLang (newLang);
		textData = lang.GetTextAsset ();
		UpdateText ();
	}

	public string GetUnitSecond(){
		return textData.unit_second;
	}

	public void UpdatePostWaveTitle(int waveNum, bool success){
		if(sr_post_wave_title != null){
			sr_post_wave_title.text = textData.sr_post_wave_title + " " + waveNum.ToString();
		}
		if(success){
			sr_post_wave_title.text += " " + textData.sr_post_wave_success;
		} else {
			sr_post_wave_title.text += " " + textData.sr_post_wave_fail;
		}

	}
}
