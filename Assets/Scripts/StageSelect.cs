﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSelect : MonoBehaviour {

	public Button[] worldButtons;
	public GameObject[] worldStages;

	void Awake(){
		showWorld (1);
	}

	public void showWorld(int worldNum){
		worldNum -= 1;
		for(int i=0; i < 6; i++){
			if(i == worldNum){
				worldButtons [worldNum].interactable = false;
				worldStages [worldNum].SetActive (true);
			} else {
				worldButtons [i].interactable = true;
				worldStages [i].SetActive (false);
			}
		}
	}
}
