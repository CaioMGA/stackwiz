﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	public AudioMixer _mixer;
	public Toggle gridToggle;

	void Start(){
		gridToggle.GetComponent<GridScript> ().Init ();
	}

	public void LoadScene(string sceneName){
		SceneManager.LoadScene (sceneName);
	}

	public void Show(GameObject go){
		go.SetActive (true);
	}

	public void Hide(GameObject go){
		go.SetActive (false);
	}

	public void ToggleInteractable(Button btn){
		btn.interactable = !btn.interactable;
	}

	public void Toggle(GameObject go){
		go.SetActive (!go.activeInHierarchy);
	}

	public void Quit(){
		Application.Quit ();
	}

	public void OpenURL(string url){
		Application.OpenURL (url);
	}

	public void Restart(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void NextScene(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex +1);
	}

	public void MusVolChange(float vol){
		_mixer.SetFloat ("VolMus", vol);
	}

	public void FxVolChange(float vol){
		_mixer.SetFloat ("VolFx", vol);
	}

	public void UpdateMusSlider(Slider sl){
		float val;
		_mixer.GetFloat ("VolMus", out val);
		sl.value = val;
	}

	public void UpdateFxSlider(Slider sl){
		float val;
		_mixer.GetFloat ("VolFx", out val);
		sl.value = val;
	}

	public void Pause(){
		GameObject.FindObjectOfType<GameLogic> ().Pause ();
	}
}
