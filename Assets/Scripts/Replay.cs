﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Replay : MonoBehaviour {
	float dropDeltatime;

	PlaceObject po;
	//public List<PlayerMove> moves;
	public PlayerMove[] array_moves;

	int curPiece = 0;
	int moveCount = 0;
	GameObject pieceHolder;

	bool isPlaying = false;
	public bool done = false;

	public void init(PlaceObject po, int moveCount){ //call for creating Replays
		this.po = po;
		this.moveCount = moveCount;
		array_moves = new PlayerMove[moveCount];
	}

	public void Go(){
		po.Drop (array_moves [curPiece].GetPieceType (), array_moves [curPiece].GetPosition (), true, pieceHolder);
		curPiece++;
	}


	public void Tick(){
		if(isPlaying){
			if(curPiece >= array_moves.Length){
				isPlaying = false;
				done = true;
			} else if(array_moves[curPiece].GetDropTime() <= dropDeltatime){
				Go ();
			} 
			dropDeltatime += Time.fixedDeltaTime;
		} 
	}

	public void Play(){// call to execute Replays
		isPlaying = true;
		done = false;
		dropDeltatime = 0;
		curPiece = 0;
		po = GameObject.FindObjectOfType<PlaceObject> ();
		pieceHolder = new GameObject ();
		pieceHolder.name = "Pieces";
	}

	public void Clear(){
		if(pieceHolder != null){
			Destroy (pieceHolder);
		}
	}

	public void Restart(){
		Clear ();
		Play ();
	}
}
