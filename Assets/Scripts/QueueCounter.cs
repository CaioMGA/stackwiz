﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QueueCounter : MonoBehaviour {

	public Animator anim;

	private int pieces = 0;
	private Text counterText;

	public void init (int pieces) {
		this.pieces = pieces;
		counterText = anim.GetComponentInChildren<Text> ();
		counterText.text = pieces.ToString ();
	}
	
	public void UpdateCounter () {
		pieces -= 1;
		counterText.text = pieces.ToString ();
		anim.SetTrigger ("UPDATE");
	}
}
