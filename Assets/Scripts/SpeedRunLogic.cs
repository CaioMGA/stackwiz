﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpeedRunLogic : MonoBehaviour {
	[Header("Objects")]
	public Sprite[] DropThumbs;
	public int[] curDropOrder; //order of objects to be stacked
	public DropList[] wavesDrops;
	[Space]
	[Header("Menu")]
	public GameObject PauseButton;
	public GameObject PostWavePanel;
	public RectTransform queueContent;
	public GameObject queueButton;
	public GameObject frameNextPiece;
	public EventSystem _eventSystem;
	[Space]
	public GameObject clockUI;
	public Text DropTimer;
	[Space]
	[Header("Audio")]
	public AudioClip PieceDrop;
	public GameObject _musicPlayer;
	[Header("Aim")]
	public GameObject aim;
	[Space]
	public GameObject ghost;
	[Header("Ghost Pieces")]
	public GameObject cube;
	public GameObject cube45;
	public GameObject rectangle;
	public GameObject rectangle90;
	public GameObject triangleUp;
	public GameObject triangleDown;
	public GameObject triangleJ;
	public GameObject triangleL;
	public GameObject sphere;
	public GameObject smallRectangle90;
	public GameObject pato;
	public GameObject porco;

	Ad _ad;
	AimAssistSettings aimAssistSettings;
	AudioSource _audioSource;
	GameObject dropHolder;
	GridScript _grid;
	List<Vector3> DropPos;
	List<float> DropDelayCount;
	PlaceObject _placeObj;
	[HideInInspector]
	QueueCounter queueCounter;
	TextHandler textHandler;

	string stageName;
	//string aimAssistSettings.aimAssistMode;

	float clockShowTime = 7; //tempo em que o relógio fica na tela
	float WinTime = 5;
	float winTimeDeltatime = 0;
	float dropTime = 0;
	float DropDelay = 2;
	float dropTimeUpdateDelay = 1;
	float dropTimeUpdateDelayDeltatime = 0;
	int unpauseDelay = 2;
	int unpauseCounter = 0;
	int nextPieceDrop;
	int piecesCount;
	int waveCount = 0;

	string gameState = "init";
	bool winTimerActive = false;
	bool waveover = false;
	bool countingDropTime = false;
	bool paused = false;
	bool unpausing = false;
	bool showingAim = false;

	//HACKS
	bool HACK_mouseDown = false;
	bool HACK_mouseUp = false;

	public void Init(){
		gameState = "instructions";
		GameObject.FindObjectOfType<GridScript>().Init ();
		dropHolder = new GameObject ();
		_grid = GameObject.FindObjectOfType<GridScript> ();
		stageName = SceneManager.GetActiveScene ().name;
		_placeObj = GetComponent<PlaceObject> ();
		textHandler = GameObject.FindObjectOfType<TextHandler> ();
		_audioSource = GetComponent<AudioSource> ();
		if(!FindMusicPlayer()){
			Instantiate (_musicPlayer);
		}
		Time.timeScale = 1;
		DropPos = new List<Vector3>();
		DropDelayCount = new List<float>();
		aimAssistSettings = GetComponent<AimAssistSettings> ();
		aimAssistSettings.aimAssistMode = aimAssistSettings.GetAssistMode ();
		aimAssistSettings.UpdateAssistModeButtons ();
		queueCounter = GetComponent<QueueCounter> ();
		queueCounter.init(curDropOrder.Length);
		Utils.Stats.AddTry ();
		Debug.Log ("Init successful");
	}

	void FixedUpdate(){
		if(gameState.Equals("init")){
			Init ();
		}
		if (gameState.Equals ("game")) {
			UpdateStagedCounters ();
			AimAssists ();
			if (_eventSystem.IsPointerOverGameObject ()) {
				//verifica se está sobre um elemento da UI
			} else if (nextPieceDrop < piecesCount) {
				if (Input.GetMouseButtonDown (0) && !HACK_mouseDown) {
					HACK_mouseDown = true;
					HACK_mouseUp = false;
					ShowAim (true, Input.mousePosition);
					Debug.Log ("Mouse DOWN");

				}
				if (Input.GetMouseButtonUp (0) && !HACK_mouseUp && HACK_mouseDown) {
					HACK_mouseDown = false;
					HACK_mouseUp = true;
					StageDrop (Input.mousePosition);
					ShowAim (false, Input.mousePosition);
					Debug.Log ("Mouse UP");
				}
			} else if (!winTimerActive) {
				StartWinTimer ();
			}
			if (countingDropTime) {
				dropTime += Time.deltaTime;
				UpdateDropTimerDisplay (false);
			}

			if (winTimerActive) {
				if (winTimeDeltatime < WinTime) {
					winTimeDeltatime += Time.deltaTime;
				} else {
					if (winTimeDeltatime < clockShowTime) {
						winTimeDeltatime += Time.deltaTime;
					} else {
						Win ();
					}
				}
			}
		} else if (gameState.Equals ("pause")) {

		} else if (gameState.Equals ("unpausing")) {

		}
	}

	public void NextWave(){
		//get drop list
		getDropList ();
		//init queue
		InitQueue ();
		//wait for drop
		winTimeDeltatime = 0;
		winTimerActive = false;

		gameState = "game";
		waveover = false;
	}

	void getDropList(){
		//set DropList to a list of drops for the current wave
		curDropOrder = wavesDrops [waveCount].list;
		piecesCount = curDropOrder.Length;
		nextPieceDrop = 0;
	}

	void StageDrop(Vector3 _pos){
		DropDelayCount.Add (0);
		DropPos.Add (_pos);
	}

	void UpdateStagedCounters(){
		int stagedCounters = DropDelayCount.Count;
		bool[] deleteCounters = new bool[stagedCounters];
		for(int i = 0; i < stagedCounters; i++){
			deleteCounters [i] = false;
			DropDelayCount [i] += 1;
			if(DropDelayCount [i] >= DropDelay){
				Drop (DropPos [0]);
				DropPos.RemoveAt (0);
				deleteCounters [i] = true;
			}
		}
		for(int j = 0; j < stagedCounters; j++){
			if(deleteCounters[j]){
				if(DropDelayCount.Count > 0){
					DropDelayCount.RemoveAt (0);
				} else {
					return;
				}
			}
		}
	}

	void Drop(Vector3 mousePos){
		if(nextPieceDrop < piecesCount && !winTimerActive){
			if(_placeObj.Drop (curDropOrder[nextPieceDrop], mousePos, false, dropHolder)){
				_audioSource.pitch *= 1.05f;
				_audioSource.PlayOneShot (PieceDrop);

				nextPieceDrop++;
				queueCounter.UpdateCounter ();
				if(queueContent.childCount > 1){
					queueContent.transform.GetChild (0).transform.GetChild (0).transform.SetParent(queueContent.transform.GetChild (1).transform);
					FrameUpdate (queueContent.transform.GetChild (1).transform.GetChild (0));
				}
				queueContent.sizeDelta = new Vector2 (((piecesCount - nextPieceDrop) * 160), 0);
				if(!countingDropTime){
					countingDropTime = true;
				}
				Destroy(queueContent.transform.GetChild (0).gameObject);
			}
		} else {
			StartWinTimer ();
		}
	}

	void StartWinTimer(){
		countingDropTime = false;
		UpdateDropTimerDisplay (true);
		Debug.Log ("Drop Time:" + dropTime);
		winTimerActive = true;
		winTimeDeltatime = 0;
		queueContent.parent.parent.gameObject.SetActive (false);
		queueButton.SetActive (false);
		clockUI.SetActive (true);
		PauseButton.SetActive (false);
	}

	public void Fail(){
		ShowAim (false, Vector3.zero);
		Debug.Log ("FAIL");
		queueButton.SetActive (false);
		clockUI.SetActive (false);
		waveover = true;
		//show WavePanel
		textHandler.UpdatePostWaveTitle (waveCount + 1, false);
		PostWavePanel.SetActive (true);
	}

	void Win(){
		if (!waveover) {
			Utils.Stats.SetStageClear (stageName);
			ShowAim (false, Vector3.zero);
			//SHOW wave time / partial time / run time
			textHandler.UpdatePostWaveTitle (waveCount + 1, true);
			PostWavePanel.SetActive (true);
			PauseButton.SetActive (false);
			clockUI.SetActive (false);
			gameState = "win";
			waveCount++;
			waveover = true;

		}
	}

	void InitQueue (){
		int drops = curDropOrder.Length;
		//queueContent.rect = new Rect (queueContent.rect.x, queueContent.rect.y, queueContent.rect.width, queueContent.rect.height);
		queueContent.sizeDelta = new Vector2 ((drops * 160), 0);
		for(int i = 0; i < drops; i++){
			GameObject thumb = new GameObject ("img");
			thumb.transform.parent = queueContent.transform;
			thumb.AddComponent<Image> ();
			thumb.GetComponent<Image> ().sprite = DropThumbs [curDropOrder [i]];
			thumb.transform.localScale = new Vector2 (1, 1);
			thumb.GetComponent<Image> ().preserveAspect = true;
		}

		GameObject nextFrame = Instantiate(frameNextPiece);
		nextFrame.transform.parent = queueContent.GetChild (0).transform;
		FrameUpdate (nextFrame.transform);
		queueButton.SetActive (true);

	}

	void FrameUpdate(Transform frame){
		frame.transform.localScale = new Vector2 (1, 1);
		frame.GetComponent<RectTransform> ().anchoredPosition = new Vector2(0, 0);
		frame.GetComponent<RectTransform> ().anchorMax = new Vector2 (1, 1);
		frame.GetComponent<RectTransform> ().anchorMin = new Vector2 (0, 0);
		frame.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.5f);
		frame.GetComponent<Image> ().preserveAspect = true;
		frame.GetComponent<RectTransform> ().sizeDelta = new Vector2(1, 1);
	}

	public void Pause(){
		paused = !paused;
		if(paused){
			Time.timeScale = 0;
			unpauseCounter = 0;
			ShowAim (false, Vector3.zero);
		} else {
			unpausing = true;
		}

		DropDelayCount.Clear();
		DropPos.Clear();
	}

	public void Unpause(){
		paused = false;
		unpausing = true;
		Time.timeScale = 1;
	}

	bool FindMusicPlayer(){
		if(GameObject.FindObjectsOfType<MusicPlayer>().Length < 1 ){
			return false;
		}
		return true;
	}

	void UpdateDropTimerDisplay(bool ignoreDelay){
		string dropTimeString = dropTime.ToString("N3");
		if(ignoreDelay){
			DropTimer.text = dropTimeString + " " + textHandler.GetUnitSecond();
		} else if(dropTimeUpdateDelayDeltatime > dropTimeUpdateDelay){
			DropTimer.text = dropTimeString + " " + textHandler.GetUnitSecond();
			dropTimeUpdateDelayDeltatime = 0;
		} else {
			dropTimeUpdateDelayDeltatime += 1;
		}
	}

	void TrackTouch (){
		if(aimAssistSettings.aimAssistMode.Equals("Red Cross")){
			aim.transform.position = Input.mousePosition;
		} else if(aimAssistSettings.aimAssistMode.Equals("Ghost Pieces")){
			_placeObj.MoveObjOnPlane(ghost, Input.mousePosition);
		} else {
			//No Assist
		}
	}
	void ShowAim (bool val, Vector3 mousePos){
		showingAim = val;
		if(aimAssistSettings.aimAssistMode.Equals("Red Cross")){
			TrackTouch ();
			aim.SetActive (val);
		} else if(aimAssistSettings.aimAssistMode.Equals("Ghost Pieces")){
			if(val){
				ghost = GetGhostPiece (mousePos);
				TrackTouch ();
			} else {
				Destroy (ghost);
			}
		} else {
			//No Assist
		}
	}

	void AimAssists(){
		if(aimAssistSettings.aimAssistMode.Equals("Red Cross")){
			if(showingAim){
				TrackTouch ();
			}
		} else if(aimAssistSettings.aimAssistMode.Equals("Ghost Pieces")){
			if(showingAim){
				TrackTouch ();
			}
		}
	}

	GameObject GetGhostPiece (Vector3 mousePos){
		string nextPiecetag;
		GameObject piece = null;
		if(nextPieceDrop >= piecesCount){
			return null;
		}
		nextPiecetag = _placeObj.GetPieceTag(curDropOrder[nextPieceDrop]);
		switch (nextPiecetag){
		case "cube45":
			piece = Instantiate (cube45);
			break;
		case "rectangle":
			piece = Instantiate (rectangle);
			break;
		case "rectangle90":
			piece = Instantiate (rectangle90);
			break;
		case "rectangleSmall90":
			piece = Instantiate (smallRectangle90);
			break;
		case "triangleUp":
			piece = Instantiate (triangleUp);
			break;
		case "triangleDown":
			piece = Instantiate (triangleDown);
			break;
		case "triangleL":
			piece = Instantiate (triangleL);
			break;
		case "triangleJ":
			piece = Instantiate (triangleJ);
			break;
		case "sphere":
			piece = Instantiate (sphere);
			break;
		case "pato":
			piece = Instantiate (pato);
			break;
		case "porco":
			piece = Instantiate (porco);
			break;
		default:
			piece = Instantiate (cube);
			break;
		}
		_placeObj.MoveObjOnPlane (piece, mousePos);
		return piece;
	}
}
