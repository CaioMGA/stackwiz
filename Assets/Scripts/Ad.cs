﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
//using UnityEngine.SceneManagement;

public class Ad : MonoBehaviour {
	public float adInterval = 300f;
	public int maxSceneCount = 10;

	float start;
	float finish;
	int sceneCount = 0;

	bool timeSet = false;

	void Awake(){
		Advertisement.Initialize ("1697962");
		DontDestroyOnLoad(transform.gameObject);
	}

	public void preSceneInit(){
		if(timeSet){
			sceneCount += 1;
			checkSceneCount ();
			checkAdInterval ();
		} else {
			timeSet = true;
			start = Time.time;
			finish = start + adInterval;
			sceneCount = 0;
		}
	}

	void checkSceneCount (){
		if(sceneCount >= maxSceneCount){
			Reset ();
		}
	}

	void checkAdInterval(){
		if(Time.time >= finish){
			Reset ();
		}
	}

	void Reset(){
		Advertisement.Show ();
		timeSet = false;
	}

	public bool isShowingAd(){
		return Advertisement.isShowing;
	}

	public void ShowRewardedVideo (){
	    var options = new ShowOptions();
	    options.resultCallback = HandleShowResult;
	    
	    Advertisement.Show("rewardedVideo", options);
	}

	void HandleShowResult (ShowResult result){
	    if(result == ShowResult.Finished) {
	        Debug.Log("Video completed!");
			//ShowSolution
			GameObject.FindObjectOfType<HintController> ().UnlockHint ();
			GameObject.FindObjectOfType<HintController> ().SolutionButtonPressed ();
	        
	    }else if(result == ShowResult.Skipped) {
			//Show "You only unlock the solution after watching the whole video"
			GameObject.FindObjectOfType<HintController> ().SolutionButtonPressed ();
	        Debug.LogWarning("Video was skipped");
	        
	    }else if(result == ShowResult.Failed) {
			//show fail panel
	        Debug.LogError("Video failed to show");
	    }
	}
}
