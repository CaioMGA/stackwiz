﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveButton : MonoBehaviour {
	Rigidbody rb;
	Vector3 defaultPos;
	Quaternion defaultRot;
	void Awake(){
		rb = GetComponent<Rigidbody> ();
		defaultPos = transform.position;
		defaultRot = transform.rotation;
	}

	public void Go(){
		rb.isKinematic = false;
	}

	public void Reset(){
		Debug.Log (this.gameObject.name + " RESET");
		//rb.velocity = Vector3.zero;
		//rb.angularVelocity = Vector3.zero;
		rb.isKinematic = true;
		transform.position = defaultPos;
		transform.rotation = defaultRot;
	}
}
