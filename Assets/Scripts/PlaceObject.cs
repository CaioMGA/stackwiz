﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceObject : MonoBehaviour {
	public GameObject[] DropObjects;//pieces prefabs
	//List<GameObject> DropList; //list of objects to be stacked

	public bool Drop(int type, Vector3 pos, bool worldPos){
		if (!worldPos) {
			Ray ray = Camera.main.ScreenPointToRay (pos);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 100)) {
				Debug.Log (hit.transform.gameObject.name);///////////////////////////
				if (hit.transform.CompareTag ("Drop Plane")) {
					Vector3 newPos = new Vector3 (hit.point.x, hit.point.y, 0);
					GameObject go = Instantiate (DropObjects [type]);
					go.transform.position = newPos;
				} else {
					//whiffed
					return false;
				}
			}
		} else {
			GameObject go = Instantiate (DropObjects [type]);
			go.transform.position = pos;
		}
		return true;
	}

	public bool Drop(int type, Vector3 pos, bool worldPos, GameObject pieceHolder){
		if (!worldPos) {
			Ray ray = Camera.main.ScreenPointToRay (pos);
			RaycastHit hit;
			RaycastHit[] hits = Physics.RaycastAll (ray, 100);
			if (hits.Length > 1) {
				//whiffed
				return false;
			}
			if (Physics.Raycast (ray, out hit, 100)) {
				if (hit.transform.CompareTag ("Drop Plane")) {
					Vector3 newPos = new Vector3 (hit.point.x, hit.point.y, 0);
					GameObject go = Instantiate (DropObjects [type]);
					go.transform.position = newPos;
					go.transform.parent = pieceHolder.transform;
				} else {
					//whiffed
					return false;
				}
			}
		} else {
			GameObject go = Instantiate (DropObjects [type]);
			go.transform.position = pos;
			go.transform.parent = pieceHolder.transform;
		}
		return true;
	}

	public bool MoveObjOnPlane(GameObject obj, Vector3 pos){
		Ray ray = Camera.main.ScreenPointToRay (pos);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, 100)){
			if(hit.transform.CompareTag("Drop Plane")){
				Vector3 newPos = new Vector3(hit.point.x, hit.point.y, 0);
				obj.transform.position = newPos;
			} else {
				//whiffed
				return false;
			}
		}
		return true;
	}

	public string GetPieceTag(int index){
		if(index > 0 && index < DropObjects.Length){
			return DropObjects [index].transform.tag;
		}
		return "";
	}

	public static Vector3 GetWorldPosition(Vector3 mousePos){
		Ray ray = Camera.main.ScreenPointToRay (mousePos);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, 100)){
			if(hit.transform.CompareTag("Drop Plane")){
				Vector3 newPos = new Vector3(hit.point.x, hit.point.y, 0);
				return newPos;
			}
		}
		return Vector3.zero;
	}

}
