﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StageInfo : System.Object {

	//public int world; not needed
	public int stage;
	public float best_time;

	/*
	public StageInfo(int w, int s, float t){
		this.world = w;
		this.stage = s;
		this.best_time = t;
	}
	*/
}
