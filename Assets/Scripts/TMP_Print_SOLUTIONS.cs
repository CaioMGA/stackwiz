﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TMP_Print_SOLUTIONS : MonoBehaviour {

	public GameObject SOLUTIONS;

	int index = 0;
	void Update(){
		if(Input.GetKeyDown(KeyCode.Space)){
			PlayMoves (SOLUTIONS.transform.GetChild (index).gameObject);
		}

		if(Input.GetKeyDown(KeyCode.KeypadPlus)){
			index++;
			if(index >= SOLUTIONS.transform.childCount ){
				index = 0;
			}
		}

		if(Input.GetKeyDown(KeyCode.KeypadMinus)){
			index--;
			if(index < 0){
				index = SOLUTIONS.transform.childCount - 1;
			}
		}
		if(Input.GetMouseButtonUp(0)){
			//Debug.Log (this.GetInstanceID() + " Mouse Up");
		} else if(Input.GetMouseButtonDown(0)){
			//Debug.Log (this.GetInstanceID() + " Mouse Down");
		}

	}

	void PlayMoves (GameObject go){
		Replay replay = go.GetComponent<Replay> ();
		Debug.Log ("===== " + index + " =====");
		Debug.Log ("Moves: " + replay.array_moves.Length);
		foreach(var playerMove in replay.array_moves){
			playerMove.PrintPlayerMove ();
		}
	}
}
