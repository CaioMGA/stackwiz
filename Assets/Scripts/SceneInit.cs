﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class SceneInit : MonoBehaviour {

	public GameObject defaultScene;
	public float extraStarTime = 10f;
	public int[] DropOrder;
	Ad _ad;
	bool sceneCreated = false;

	void Start(){
		_ad = FindObjectOfType<Ad> ();
		_ad.preSceneInit ();

	}

	void Update(){
		if(!_ad.isShowingAd() && !sceneCreated){
			sceneCreated = true;
			Instantiate (defaultScene);
			GameObject.FindObjectOfType<GridScript>().Init ();
			GameLogic _gl = FindObjectOfType<GameLogic> ();
			_gl.DropOrder = this.DropOrder;
			_gl.extraStarTime = this.extraStarTime;
			_gl.Init ();
		}
	}
}
