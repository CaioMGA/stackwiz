﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

// DOES NOT WORK ON ANDROID MOBILE

public class LanguagePack : MonoBehaviour {
	public TextAsset txt;
	string lang;
	public bool loadComplete = false;

	void Start(){
		DontDestroyOnLoad (this.gameObject);
		lang = GetCurLang ();
		txt = loadJson ();
	}

	public string GetCurLang(){
		return PlayerPrefs.GetString ("curLang");
	}

	public void SetCurLang(string l){
		loadComplete = false;
		Debug.Log ("Language changed to " + l);
		PlayerPrefs.SetString ("curLang", l);
		lang = l;
		txt = loadJson ();
	}

	public TextAsset loadJson(){
		string dataFileName = lang + ".json";
		//string dataFileName = "pt-br.json";
		string filePath = Path.Combine (Application.streamingAssetsPath, dataFileName);
		if(File.Exists(filePath)){
			string jsonData = File.ReadAllText (filePath);
			TextAsset loadedData = JsonUtility.FromJson<TextAsset> (jsonData);
			//TextAsset loadedData = JsonUtility.FromJson(jsonData, TextAsset);
			loadComplete = true;
			return loadedData;
		} else {
			Debug.LogError ("JSON Não encontrado");
		}
		return null;

	}

}
