﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AimAssistSettings : MonoBehaviour {

	public Button btnRedCross;
	public Button btnGhostPieces;
	public Button btnNoAssist;

	public string aimAssistMode;

	void Start(){
		aimAssistMode = GetAssistMode ();
	}

	public void SetAssistMode(string mode){
		PlayerPrefs.SetString ("AimAssistMode", mode);
		aimAssistMode = mode;
		UpdateAssistModeButtons ();
	}

	public string GetAssistMode(){
		string newMode = "No Assist";
		if(PlayerPrefs.HasKey("AimAssistMode")){
			newMode = PlayerPrefs.GetString("AimAssistMode");
		}
		aimAssistMode = newMode;
		return newMode;
	}

	public void UpdateAssistModeButtons(){
		if(aimAssistMode.Equals("Red Cross")){
			btnRedCross.interactable = false;
			btnGhostPieces.interactable = true;
			btnNoAssist.interactable = true;
		} else if(aimAssistMode.Equals("Ghost Pieces")){
			btnRedCross.interactable = true;
			btnGhostPieces.interactable = false;
			btnNoAssist.interactable = true;
		} else {
			btnRedCross.interactable = true;
			btnGhostPieces.interactable = true;
			btnNoAssist.interactable = false;
		}
	}
}
